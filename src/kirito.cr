require "kemal"
require "./upload"
require "./fetch"

before_all "/foo" do |env|
  env.response.content_type = "application/json"
end

error 404 do
  {error: 404}
end

get "/" do
  "{}"
end

Dir.mkdir_p "uploads"
Kemal.run 3001
