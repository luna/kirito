require "kemal"

get "/img/:image" do |env|
  image = env.params.url["image"]
  send_file env, "./uploads/#{image}"
end
