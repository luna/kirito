require "kemal"

ALPHABET = "abcdefghijklmnopqrstuvwxyz0123456789".split("")

def gen_id()
  letters = ALPHABET.sample 6
  res = ""

  letters.each do |letter|
    res += letter
  end

  res
end

post "/api/upload" do |env|
  files = env.params.files
  puts files
  file_key = files.keys[0]
  given_file = env.params.files[file_key]

  given_filename = given_file.filename.as(String)
  extension = File.extname(given_filename)

  genned_id = gen_id
  file_path = ::File.join ["./uploads/", "#{genned_id}#{extension}"]

  File.open(file_path, "w") do |f|
    IO.copy(given_file.tempfile, f)
  end

  {
    id: genned_id,
  }.to_json
end
